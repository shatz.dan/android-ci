FROM openjdk:17-alpine
RUN apk update
RUN apk add wget tar unzip libstdc++6 gcompat bash curl

  # Setup path as android_home for moving/exporting the downloaded sdk into it
ENV ANDROID_HOME="${PWD}/android-home"
ENV ANDROID_SDK_TOOLS="6514223"
ENV ANDROID_COMPILE_SDK="33"
ENV ANDROID_BUILD_TOOLS="33.0.0"
  # Create a new directory at specified location
RUN install -d $ANDROID_HOME
  # Here we are installing androidSDK tools from official source,
  # (the key thing here is the url from where you are downloading these sdk tool for command line, so please do note this url pattern there and here as well)
  # after that unzipping those tools and
  # then running a series of SDK manager commands to install necessary android SDK packages that'll allow the app to build
RUN wget --output-document=$ANDROID_HOME/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip
  # move to the archive at ANDROID_HOME
RUN cd $ANDROID_HOME && unzip -d cmdline-tools cmdline-tools.zip && cd ..
ENV PATH=$PATH:${ANDROID_HOME}/cmdline-tools/tools/bin/

  # Nothing fancy here, just checking sdkManager version
RUN sdkmanager --version

  # use yes to accept all licenses
RUN yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses || true
RUN sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-${ANDROID_COMPILE_SDK}"
RUN sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools"
RUN sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${ANDROID_BUILD_TOOLS}"
